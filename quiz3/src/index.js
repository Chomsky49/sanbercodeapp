import React from 'react'
import { StyleSheet, Text, View } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';

import Splash from './splash'
import Home from './Home'
import Login from './login'
import Register from './Register'


const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();


const index = () => {
    return (
        <NavigationContainer >
            <Stack.Navigator initialRouteName={Splash}>
                <Stack.Screen name="Splash" component={Splash}/>
                <Stack.Screen name="Home" component={Home} />
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="Register" component={Register} />
            </Stack.Navigator>
      </NavigationContainer>
    )
    }