import { registerRootComponent } from 'expo';
import React, {Component} from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    Image, 
    Platform,
    TouchableOpacity,
    FlatList,
    ScrollView,
    TextInput,
   } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';


export default class App extends Component {
    render(){
        return(
            
            <View style = {styles.constructor}>
                <View style = {styles.header}>
                <Image source = {require('./logo.png')} style = {{width : 300, height : 90}} />
                </View>
                {/* <View style = {styles.logonama}> */}
                <View style = {{
                    paddingVertical: 15,
                    paddingHorizontal: 3,
                    flexDirection: "row",
                    alignItems: "center"
                } }>
                <Icon style = {styles.gambar} name = 'account-circle' size = {50} color = 'blue'></Icon> 
                <View style = {{
                    flexDirection : 'column',
                }}>
                <Text style = {styles.namasapa}>Hai,</Text>
                <Text style = {styles.namaasli}>Abiyan Bagus Baskoro</Text>
                </View>
                </View>
                <Text style = {styles.skill}>SKILL</Text>
                <View style = {styles.kotak}>
                    <TouchableOpacity style = {styles.kotakskill}>
                    <Text style = {styles.katakotak}>Library/Framework</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style = {styles.kotakskill}>
                    <Text style = {styles.katakotak}>Bahasa Pemograman</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style = {styles.kotakskill}>
                    <Text style = {styles.katakotak}>Teknologi</Text>
                    </TouchableOpacity>
                </View>
                <ScrollView>

                <TouchableOpacity style = {styles.listkotak}>
                    <View style = {{
                        flexDirection: "row",
                    }}>
                    <Icon style = {styles.gambarskill} name = 'facebook' size = {80}></Icon>
                    <View style = {{
                        flexDirection : 'column',
                        justifyContent : 'center',
                        marginLeft : 30,
                    }}>
                    <Text style = {styles.skillasli}>React Native</Text>
                    <Text style = {styles.library}>Library / Framework</Text>
                    <Text style = {styles.angka} color = 'white' >50%</Text>
                    </View>
                    <View style = {{
                        marginLeft : 30,
                        alignItems : 'center'
                   }}>
                    <Icon style = {styles.logo} name = 'keyboard-arrow-right' size = {100}></Icon>
                   </View>
                   </View>
                   </TouchableOpacity>

                   <TouchableOpacity style = {styles.listkotak}>
                    <View style = {{
                        flexDirection: "row",
                    }}>
                    <Icon style = {styles.gambarskill} name = 'facebook' size = {80}></Icon>
                    <View style = {{
                        flexDirection : 'column',
                        justifyContent : 'center',
                        marginLeft : 30,
                    }}>
                    <Text style = {styles.skillasli}>Laravel</Text>
                    <Text style = {styles.library}>Library / Framework</Text>
                    <Text style = {styles.angka}>100%</Text>
                    </View>
                    <View style = {{
                        marginLeft : 30
                   }}>
                    <Icon style = {styles.logo} name = 'keyboard-arrow-right' size = {100}></Icon>
                   </View>
                   </View>
                   </TouchableOpacity>

                   <TouchableOpacity style = {styles.listkotak}>
                    <View style = {{
                        flexDirection: "row",
                    }}>
                    <Icon style = {styles.gambarskill} name = 'facebook' size = {80}></Icon>
                    <View style = {{
                        flexDirection : 'column',
                        justifyContent : 'center',
                        marginLeft : 30,
                    }}>
                    <Text style = {styles.skillasli}>Javascript</Text>
                    <Text style = {styles.library}>Bahasa Pemograman</Text>
                    <Text style = {styles.angka}>30%</Text>
                    </View>
                    <View style = {{
                        marginLeft : 30,
                        alignItems : 'center'
                   }}>
                    <Icon style = {styles.logo} name = 'keyboard-arrow-right' size = {100}></Icon>
                   </View>
                   </View>
                   </TouchableOpacity>

                   <TouchableOpacity style = {styles.listkotak}>
                    <View style = {{
                        flexDirection: "row",
                    }}>
                    <Icon style = {styles.gambarskill} name = 'facebook' size = {80}></Icon>
                    <View style = {{
                        flexDirection : 'column',
                        justifyContent : 'center',
                        marginLeft : 30,
                    }}>
                    <Text style = {styles.skillasli}>Python</Text>
                    <Text style = {styles.library}>Bahasa Pemograman</Text>
                    <Text style = {styles.angka}>70%</Text>
                    </View>
                    <View style = {{
                        marginLeft : 30
                   }}>
                    <Icon style = {styles.logo} name = 'keyboard-arrow-right' size = {100}></Icon>
                   </View>
                   </View>
                   </TouchableOpacity>

                   <TouchableOpacity style = {styles.listkotak}>
                    <View style = {{
                        flexDirection: "row",
                    }}>
                    <Icon style = {styles.gambarskill} name = 'facebook' size = {80}></Icon>
                    <View style = {{
                        flexDirection : 'column',
                        justifyContent : 'center',
                        marginLeft : 30,
                    }}>
                    <Text style = {styles.skillasli}>Git</Text>
                    <Text style = {styles.library}>Teknologi</Text>
                    <Text style = {styles.angka}>75%</Text>
                    </View>
                    <View style = {{
                        marginLeft : 100
                   }}>
                    <Icon style = {styles.logo} name = 'keyboard-arrow-right' size = {100}></Icon>
                   </View>
                   </View>
                   </TouchableOpacity>

                   <TouchableOpacity style = {styles.listkotak}>
                    <View style = {{
                        flexDirection: "row",
                    }}>
                    <Icon style = {styles.gambarskill} name = 'facebook' size = {80}></Icon>
                    <View style = {{
                        flexDirection : 'column',
                        justifyContent : 'center',
                        marginLeft : 30,
                    }}>
                    <Text style = {styles.skillasli}>Gitlab</Text>
                    <Text style = {styles.library}>Teknologi</Text>
                    <Text style = {styles.angka}>60%</Text>
                    </View>
                    <View style = {{
                        marginLeft : 100
                   }}>
                    <Icon style = {styles.logo} name = 'keyboard-arrow-right' size = {100}></Icon>
                   </View>
                   </View>
                   </TouchableOpacity>
                </ScrollView>
                   
               

                </View>

            
        )
    }
}

const styles = StyleSheet.create({
    constructor :{
        flex : 1,
        backgroundColor: '#fff',

    },
    header :{
        alignSelf : 'flex-end',
        marginTop : 5,
    },
    namasapa : {
        fontSize : 15
    },
    namaasli : {
        color : 'blue',
        fontSize : 18
    },
    skill : {
        fontSize : 40,
        marginLeft : 5,
        borderBottomWidth : 5,
        borderBottomColor : '#00ffff'   
    },
    kotak : {
        alignItems : 'center',
        flexDirection : 'row',
    },
    kotakskill : {
        alignItems : 'center',
        backgroundColor : '#00ffff',
        borderRadius : 5,
        width : 120,
        height : 20,
        marginTop : 8,
        marginLeft : 2
    },
  
    katakotak : {
        fontSize : 12,
    },
    listkotak : {
        backgroundColor : '#B4E9FF',
        borderRadius : 5,
        width : 320,
        height : 100,
        marginTop : 8,
        marginLeft : 15,
    },
    gambarskill : {
        alignSelf : 'stretch',
        marginLeft : 6,
    },
    angka : {
        fontSize : 30,
        alignSelf : 'flex-end',
        // justifyContent : 'center'
    },
    skillasli :{
        fontSize : 24,
        color : '#003366'
    },
    library :{
        fontSize : 16,
        color : '#3EC6FF'
    },
    // angka : {
    //     color : 
    // }
  




})