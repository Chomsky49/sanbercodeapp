import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { 
  StyleSheet, 
  Text, 
  View, 
  Image, 
  Platform,
  TouchableOpacity,
  FlatList,
 } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Video from './Tugas/Tugas12/videoItem'
import Data from './Tugas/Tugas12/data.json'

// import Yotube from './Tugas/Tugas12/app'


export default function App() { 
  return (
    <View style = {styles.container}>
      <View style = {styles.navBar}> 
        <Image source ={require('./Tugas/Tugas12/logo.png')} style = {{width:98, height : 22}} />
        <View style = {styles.rightNav}>
          <TouchableOpacity>
          <Icon style = {styles.navItem} name ="search" size = {25}/>
          </TouchableOpacity>
          
          <TouchableOpacity>
          <Icon style = {styles.navItem} name ="account-circle" size = {25}/>
          </TouchableOpacity>
        </View>
      </View>
      <View style = {styles.body}>
        <Video video = {Data.items[0]}/>
          <FlatList
          keyExtractor ={(item) => item.id}
          Data = {Data.items}
          renderItem = {(video) =><Video Video ={video.item}/>}
          
    /> 
    </View>
    <View style = {styles.tabBar}>
      <TouchableOpacity style = {styles.tabItem}>
        <Icon name = "home" size = {25}/>
        <Text style = {styles.tabTitle}>Home</Text>
      </TouchableOpacity>

      <TouchableOpacity style = {styles.tabItem}>
        <Icon  name = 'whatshot' size = {25}/>
        <Text style = {styles.tabTitle}>Trending  </Text>
      </TouchableOpacity>

      <TouchableOpacity style = {styles.tabItem}>
        <Icon  name = 'subscription' size = {25}/>
        <Text style = {styles.tabTitle}>subscription</Text>
      </TouchableOpacity>

      <TouchableOpacity style = {styles.tabItem}>
        <Icon  name = 'folder' size = {25}/>
        <Text style ={styles.tabTitle}>Library</Text>
      </TouchableOpacity>
    </View>
    </View>
    
  );
}

const styles = StyleSheet.create({
  container: {
      flex:1,
      paddingTop :40, // mengatur letak bar yang terdapat di atas
  },
  navBar : {
      height : 55, // tinggi/panjang bar atas pada logo youtub
      backgroundColor : 'white', // background colour
      elevation : 3, //shadow bar atas pada logoyoutube
      paddingHorizontal :15, // jarak object pada bar
      flexDirection : 'row',
      alignItems : 'center',
      justifyContent : 'space-between' // memisahkan logo dan "search simbol" menjadi ujung-ujung
  },
  rightNav :{
      flexDirection : 'row'
  },
  navItem :{
    marginLeft : 25,
  },
  body :{
    flex : 1,
  },
  tabBar : {
    height : 60,
    backgroundColor : 'white',
    borderTopWidth : 0.5, // tebal pemisah kotak yang dibawah
    borderColor : 'black', // kotak yang ada di bawah
    flexDirection : 'row',
    justifyContent : 'space-around'
  },
  tabItem : {
    alignItems : 'center',
    justifyContent : 'center',

  },
  tabTitle:{
    fontSize : 11,
    color : '#3C3C3C',
    paddingTop : 4, 
  }


})


// export default App;