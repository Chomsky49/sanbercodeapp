import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Login from './Tugas-3/login'
import Regis from './Tugas-3/register'
import Screen from './Tugas-3/aboutscreen'

export default function App() {
  return (
    <Screen />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
