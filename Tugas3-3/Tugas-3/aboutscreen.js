import React, {Component} from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    Image, 
    Platform,
    TouchableOpacity,
    FlatList,
   } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class App extends Component { 
    render(){
        return (
            <View style = {styles.container}>
                <View style = {styles.top}>
                    <Text style = {styles.ktatas}>Tentang Saya</Text>
                    <Icon style = {styles.gambar} name = "perm-identity" size = {150} />
                    <Text style = {styles.kttengah}>Abiyan Bagus Baskoro</Text>
                    <Text style = {styles.ktdev}>React Native Developer</Text>                  
                </View>
                <View style = {styles.body}>
                    <Text style = {styles.portofolio}>Portofolio</Text>
                    <View style = {styles.logo}>
                        <Icon style = {styles.gilab} name = 'android' size = {50}/>
                        <Text style = {styles.kata}>@Chomsky49</Text>

                        <Icon style = {styles.gilab} name = 'pan-tool' size = {50}></Icon>

                        <Icon style = {styles.gi}></Icon>

                    </View>

                </View>
                <View style = {styles.bottom}>

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    top :{
        flex : 1,
        paddingTop : 90,
        alignItems : 'center',
        justifyContent : 'center',
    },
    ktatas : {
        paddingTop : 50,
        fontSize : 30,
    },
    gambar : {
        marginTop : 10,
        color : 'grey',
    },
    kttengah :{
        fontSize : 30,
        marginTop : 10,
        color : '#00008B'
    },
    ktdev : {
        fontSize : 20,
        marginTop : 20,
        color : '#0000FF'
    },
    body : {
        flex : 1,
    },
    portofolio :{
        marginTop : 50,
        paddingTop : 40,
        marginLeft : 10,
        borderBottomWidth : 1,
        fontSize : 20,
        color : 'blue',
    },
    logo :{
        flexDirection : 'row',
        marginTop : 10,
        justifyContent : 'space-around',
        alignItems : 'center',
    },
    bottom : {
        flex : 1,
    },

})