import { StatusBar } from 'expo-status-bar';
import React, {Component} from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    Image, 
    Platform,
    TouchableOpacity,
    FlatList,
    TextInput,
    Button,
   } from 'react-native';

export default class App extends Component { 
    render(){
        return (
            <View style={styles.container}>
              <View style = {styles.body}>
                <Image source = {require('./logo.png')} style = {{width : 300, height : 80}} />
                <Text style = {styles.tabTitle}>Register</Text>
              </View>
              <View style = {styles.tengah}>
                <Text style = {styles.tabMiddle}>Username</Text>
                <TouchableOpacity>
                <View style = {styles.kotak}/>
                </TouchableOpacity>
                <Text style = {styles.tabMiddle}>Email</Text>
                <TouchableOpacity>
                <View style = {styles.kotak}/>
                </TouchableOpacity>
                <Text style = {styles.tabMiddle}>Password</Text>
                <TouchableOpacity>
                <View style = {styles.kotak}/>
                </TouchableOpacity>
                <Text style = {styles.tabMiddle}>Ulangi Password</Text>
                <TouchableOpacity>
                <View style = {styles.kotak}/>
                </TouchableOpacity>
              </View>
              <View style = {styles.bawah}>
                <TouchableOpacity style = {styles.btnlogin}>
                  <Text style = {styles.tabBottom}>Login</Text>
                </TouchableOpacity>
                <Text style = {styles.atau}>Atau</Text>
                <TouchableOpacity style = {styles.buttonlogin2}>
                  <Text style = {styles.tabBottom}>Daftar</Text>
                </TouchableOpacity>
              </View>
          
            </View>
            );
        }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  body : {
      flex : 1,
      paddingTop : 30,
      flexDirection : 'column',
      alignItems : 'center',
      justifyContent : 'center'
  },
  tabTitle : {
    paddingTop : 100,
    fontSize : 20,
    color : '#003366'
  },
  tengah :{
    flex : 1,
    paddingLeft : 10,
  },
  tabMiddle : {
      fontSize : 20,
  },
  kotak : {
      width : 325,
      height : 35,
      backgroundColor : 'white',
      borderWidth : 2,
      borderColor : 'blue',
   
  },
  bawah : {
        flex : 1,
        alignItems : 'center',
        marginTop : 20,
  },
  btnlogin: {
      alignItems : 'center',
      backgroundColor : '#003366',
      padding : 10,
      borderRadius : 16,
      marginTop : 10,
      marginHorizontal : 50,
      marginBottom : 10,
      width : 140,
  },
  tabBottom :{
    fontSize : 20,
    color : 'white',
    alignItems : 'center',
    flexDirection : 'column'

  },
  atau :{
    fontSize : 20,
    color : 'blue',
    alignItems : "center",
    alignItems : 'center'
  },
  buttonlogin2 : {
    alignItems : 'center',
      backgroundColor : '#003366',
      padding : 10,
      borderRadius : 16,
      marginTop : 10,
      marginHorizontal : 50,
      marginBottom : 10,
      width : 140,
  }



});

